# About

The purpose of the web version is for the DM

# Current Features

- Managing Campaigns
  - Store multiple campaigns
  - Manage created campaigns
  - Add notes
- Managing Monsters/ Enemies
  - Contains current monsters
  - Manage custom monsters
- Managing Characters
  - Contains current characters
  - Manage custom characters
- Managing Items
  - Contains current items
- Possibly Managing Maps
  - List current maps
- Wikis for the created campaigns
  - Write articles of locations, people etc
  - Article of your characters story and choices in the world created
- Draw Maps
  - Search locations
  - Waypoints
  - Link larger maps to smaller maps
- Help
  - Suggestions on world design

# Planned Features

- Managing Monsters
  - Create custom monsters
  - Modify monsters
- Managing Characters
  - Create custom characters
  - Modify characters
- Managing Items
  - Manage custom items
- Possibly Managing Maps
  - Modify maps

# Project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
