export interface Spells {
    index:         number;
    name:          string;
    desc:          string[];
    higher_level?: string[];
    page:          string;
    range:         Range;
    components:    Component[];
    material?:     string;
    ritual:        Concentration;
    duration:      string;
    concentration: Concentration;
    casting_time:  CastingTime;
    level:         number;
    school:        School;
    classes:       School[];
    subclasses:    School[];
}

export enum CastingTime {
    The10Minutes = "10 minutes",
    The12Hours = "12 hours",
    The1Action = "1 action",
    The1BonusAction = "1 bonus action",
    The1Hour = "1 hour",
    The1Minute = "1 minute",
    The1Reaction = "1 reaction",
    The24Hours = "24 hours",
    The8Hours = "8 hours",
}

export interface School {
    name: Name;
}

export enum Name {
    Abjuration = "Abjuration",
    Bard = "Bard",
    Cleric = "Cleric",
    Conjuration = "Conjuration",
    Devotion = "Devotion",
    Divination = "Divination",
    Druid = "Druid",
    Enchantment = "Enchantment",
    Evocation = "Evocation",
    Fiend = "Fiend",
    Illusion = "Illusion",
    Land = "Land",
    Life = "Life",
    Lore = "Lore",
    Necromancy = "Necromancy",
    Paladin = "Paladin",
    Ranger = "Ranger",
    Sorcerer = "Sorcerer",
    Transmutation = "Transmutation",
    Warlock = "Warlock",
    Wizard = "Wizard",
}

export enum Component {
    M = "M",
    S = "S",
    V = "V",
}

export enum Concentration {
    No = "no",
    Yes = "yes",
}

export enum Range {
    Self = "Self",
    Sight = "Sight",
    Special = "Special",
    The100Feet = "100 feet",
    The10Feet = "10 feet",
    The120Feet = "120 feet",
    The150Feet = "150 feet",
    The1Mile = "1 mile",
    The300Feet = "300 feet",
    The30Feet = "30 feet",
    The500Feet = "500 feet",
    The500Miles = "500 miles",
    The5Feet = "5 feet",
    The60Feet = "60 feet",
    The90Feet = "90 feet",
    Touch = "Touch",
    Unlimited = "Unlimited",
}
