import { Component, OnInit, Input } from "@angular/core";
import { Campaign } from "../campaign";
import { FormGroup } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { CampaignService } from "../campaign.service";

@Component({
  selector: "app-campaign-detail",
  templateUrl: "./campaign-detail.component.html",
  styleUrls: ["./campaign-detail.component.css"]
})
export class CampaignDetailComponent implements OnInit {
  options: FormGroup;

  @Input()
  campaign: Campaign;

  constructor(private route: ActivatedRoute, private campaignService: CampaignService, private location: Location) {}

  ngOnInit() {
    this.getCampaign();
  }

  getCampaign(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.campaignService.getCampaign(id).subscribe(campaign => (this.campaign = campaign));
  }

  goBack(): void {
    this.location.back();
  }
}
