import { Component, OnInit } from "@angular/core";
import { Campaign } from "./campaign";
import { CampaignService } from "./campaign.service";

@Component({
  selector: "app-campaigns",
  templateUrl: "./campaigns.component.html",
  styleUrls: ["./campaigns.component.css"]
})
export class CampaignsComponent implements OnInit {
  campaigns: Campaign[];
  selectedCampaign: Campaign;

  constructor(private campaignService: CampaignService) {}

  getCampaigns(): void {
    this.campaignService
      .getCampaigns()
      .subscribe(campaigns => (this.campaigns = campaigns));
  }

  ngOnInit() {this.getCampaigns();}

  onSelect(campaign: Campaign): void {
    this.selectedCampaign = campaign;
  }
}
