import { Campaign } from "./campaign";

export const CAMPAIGNS: Campaign[] = [
  {
    id: 1,
    name: "Windstorm",
    description: "Windstorm is a campaign",
    date_created: "2/11/2018",
    characters: [1],
    monster: [21, 15, 11, 8, 5],
    equipment: [2, 8, 11]
  },
  {
    id: 2,
    name: "Raiders",
    description: "Raiders is a campaign",
    date_created: "3/11/2018",
    characters: [2],
    monster: [2, 5, 1, 8, 11],
    equipment: [2, 11]
  }
];
