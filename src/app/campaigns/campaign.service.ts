import { Injectable } from "@angular/core";

import { Observable, of } from "rxjs";

import { Campaign } from "./campaign";
import { CAMPAIGNS } from "./mock-campaigns";

import { MessageService } from "../message.service";

@Injectable({
  providedIn: "root"
})
export class CampaignService {
  constructor(private messageService: MessageService) {}

  getCampaigns(): Observable<Campaign[]> {
    // TODO: send the message _after_ fetching the heroes
    this.messageService.add("CampaignService: fetched campaigns");
    return of(CAMPAIGNS);
  }

  getCampaign(id: number): Observable<Campaign> {
    this.messageService.add(`CampaignService: fetched campaign id=${id}`);
    return of(CAMPAIGNS.find(campaign => campaign.id === id));
  }
}
