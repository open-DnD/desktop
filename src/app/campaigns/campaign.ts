export class Campaign {
  id: number;
  name: string;
  description: string;
  date_created: string;
  characters?: Number[];
  equipment?: Number[];
  monster?: Number[];
}
