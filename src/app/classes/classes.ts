export interface Classes {
    index:               number;
    name:                string;
    hit_die:             number;
    proficiency_choices: ProficiencyChoice[];
    proficiencies:       Proficiency[];
    saving_throws:       Proficiency[];
    starting_equipment:  ClassLevels;
    class_levels:        ClassLevels;
    subclasses:          Proficiency[];
    spellcasting:        ClassLevels;
}

export interface ClassLevels {
    class?: string;
}

export interface Proficiency {
    name: string;
}

export interface ProficiencyChoice {
    choose: number;
    type:   Type;
    from:   Proficiency[];
}

export enum Type {
    Proficiencies = "proficiencies",
}
