import { Component, OnInit } from "@angular/core";
import { Equipment } from "../equipment/equipment";
import { EquipmentService } from "../equipment/equipment.service";
import { Character } from "../characters/character";
import { CharacterService } from "../characters/character.service";
import { Campaign } from "../campaigns/campaign";
import { CampaignService } from "../campaigns/campaign.service";
import { Monster } from "../monsters/monster";
import { MonsterService } from "../monsters/monster.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  campaigns: Campaign[];
  items: Equipment[];
  characters: Character[];
  monsters: Monster[];
  constructor(
    private campaignService: CampaignService,
    private characterService: CharacterService,
    private equipmentService: EquipmentService,
    private monsterService: MonsterService
  ) {}

  getCampaigns(): void {
    this.campaignService.getCampaigns().subscribe(campaigns => (this.campaigns = campaigns.slice(0, 4)));
  }

  getItems(): void {
    this.equipmentService.getItems().subscribe(items => (this.items = items.slice(0, 4)));
  }

  getCharacters(): void {
    this.characterService.getCharacters().subscribe(characters => (this.characters = characters.slice(0, 4)));
  }

  getMonsters(): void {
    this.monsterService.getMonsters().subscribe(monsters => (this.monsters = monsters.slice(0, 4)));
  }

  ngOnInit() {
    this.getCampaigns();
    this.getItems();
    this.getCharacters();
    this.getMonsters();
  }
}
