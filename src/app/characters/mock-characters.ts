import { Character } from "./character";

export const CHARACTERS: Character[] = [
  {
    id: 1,
    name: "Test",
    Character_Class: "Druid",
    age: "24",
    sex: "Male",
    race: "Human",
    alignment: "Neutral",
    background: "He is a test character to see if works"
  },
  {
    id: 2,
    name: "Test2",
    Character_Class: "Druid",
    age: "24",
    sex: "Male",
    race: "Human",
    alignment: "Neutral",
    background: "He is a test character to see if works"
  }
];
