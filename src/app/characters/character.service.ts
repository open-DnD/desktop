import { Injectable } from "@angular/core";

import { Observable, of } from "rxjs";

import { Character } from "./character";
import { CHARACTERS } from "./mock-characters";

import { MessageService } from "../message.service";

@Injectable({
  providedIn: "root"
})
export class CharacterService {
  constructor(private messageService: MessageService) {}

  getCharacters(): Observable<Character[]> {
    // TODO: send the message _after_ fetching the heroes
    this.messageService.add("CharacterService: fetched characters");
    return of(CHARACTERS);
  }

  getCharacter(id: number): Observable<Character> {
    this.messageService.add(`CharacterService: fetched character id=${id}`);
    return of(CHARACTERS.find(character => character.id === id));
  }

  addCharacter() {}

  removeCharacter(id: number) {}
}
