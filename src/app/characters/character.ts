export class Character {
  id: number;
  name: string;
  race?: string;
  age?: string;
  sex?: string;
  Character_Class: string;
  alignment: string;
  background?: string;
  notes?: string;
}
