export interface Equipment {
  index: number;
  name: string;
  equipment_category: EquipmentCategory;
  weapon_category?: WeaponCategory;
  weapon_range?: WeaponRange;
  category_range?: CategoryRange;
  cost: Cost;
  damage?: Damage;
  range?: Range;
  weight?: number;
  properties?: DamageType[];
  throw_range?: Range;
  "2h_damage"?: Damage;
  special?: string[];
  armor_category?: string;
  armor_class?: ArmorClass;
  str_minimum?: number;
  stealth_disadvantage?: boolean;
  gear_category?: GearCategory;
  desc?: string[];
  contents?: Content[];
  tool_category?: ToolCategory;
  vehicle_category?: VehicleCategory;
  speed?: Cost;
  capacity?: string;
}

export interface Damage {
  dice_count: number;
  dice_value: number;
  damage_type: DamageType;
}

export interface DamageType {
  name: string;
}

export interface ArmorClass {
  base: number;
  dex_bonus: boolean;
  max_bonus: number | null;
}

export enum CategoryRange {
  MartialMelee = "Martial Melee",
  MartialRanged = "Martial Ranged",
  SimpleMelee = "Simple Melee",
  SimpleRanged = "Simple Ranged"
}

export interface Content {
  quantity: number;
}

export interface Cost {
  quantity: number;
  unit: Unit;
}

export enum Unit {
  Cp = "cp",
  FtRound = "ft/round",
  Gp = "gp",
  Mph = "mph",
  SP = "sp"
}

export enum EquipmentCategory {
  AdventuringGear = "Adventuring Gear",
  Armor = "Armor",
  MountsAndVehicles = "Mounts and Vehicles",
  Tools = "Tools",
  Weapon = "Weapon"
}

export enum GearCategory {
  Ammunition = "Ammunition",
  ArcaneFocus = "Arcane focus",
  DruidicFocus = "Druidic focus",
  EquipmentPack = "Equipment Pack",
  HolySymbol = "Holy Symbol",
  Kit = "Kit",
  StandardGear = "Standard Gear"
}

export interface Range {
  normal: number;
  long: number | null;
}

export enum ToolCategory {
  ArtisanSTools = "Artisan's Tools",
  GamingSets = "Gaming Sets",
  MusicalInstrument = "Musical Instrument",
  OtherTools = "Other Tools"
}

export enum VehicleCategory {
  MountsAndOtherAnimals = "Mounts and Other Animals",
  TackHarnessAndDrawnVehicles = "Tack, Harness, and Drawn Vehicles",
  WaterborneVehicles = "Waterborne Vehicles"
}

export enum WeaponCategory {
  Martial = "Martial",
  Simple = "Simple"
}

export enum WeaponRange {
  Melee = "Melee",
  Ranged = "Ranged"
}
