import { Injectable } from "@angular/core";

import { Observable, of } from "rxjs";

import { Equipment } from "./equipment";
import { EQUIPMENT } from "./mock-equipment";

import { MessageService } from "../message.service";

@Injectable({
  providedIn: "root"
})
export class EquipmentService {
  constructor(private messageService: MessageService) {}

  getItems(): Observable<Equipment[]> {
    // TODO: send the message _after_ fetching the heroes
    this.messageService.add("ItemsService: fetched items");
    return of(EQUIPMENT);
  }

  getItem(id: number): Observable<Equipment> {
    this.messageService.add(`ItemService: fetched item id=${id}`);
    return of(EQUIPMENT.find(item => item.index === id));
  }

  addEquipment(){

  }

  removeEquipment(id: Number){

  }
}
