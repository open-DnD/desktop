import { Component, OnInit, Input } from "@angular/core";
import { Equipment } from "../equipment";
import { FormGroup } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { EquipmentService } from "../equipment.service";
@Component({
  selector: 'app-equipment-detail',
  templateUrl: './equipment-detail.component.html',
  styleUrls: ['./equipment-detail.component.css']
})
export class EquipmentDetailComponent implements OnInit {
  options: FormGroup;

  @Input()
  item: Equipment;

  constructor(private route: ActivatedRoute, private itemService: EquipmentService, private location: Location) {}

  ngOnInit() {
    this.getItem();
  }

  getItem(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.itemService.getItem(id).subscribe(item => (this.item = item));
  }

  goBack(): void {
    this.location.back();
  }

}
