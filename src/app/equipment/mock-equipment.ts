import {
  Equipment,
  Damage,
  DamageType,
  ArmorClass,
  CategoryRange,
  Content,
  Cost,
  EquipmentCategory,
  Unit,
  GearCategory,
  WeaponCategory,
  WeaponRange,
  VehicleCategory,
  ToolCategory
} from "./equipment";

export const EQUIPMENT: Equipment[] = [
  {
    index: 1,
    name: "Club",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.SimpleMelee,
    cost: {
      quantity: 1,
      unit: Unit.SP
    },
    damage: {
      dice_count: 1,
      dice_value: 4,
      damage_type: {
        name: "Bludgeoning"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 2,
    properties: [
      {
        name: "Light"
      },
      {
        name: "Monk"
      }
    ]
  },
  {
    index: 2,
    name: "Dagger",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.SimpleMelee,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 4,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 1,
    properties: [
      {
        name: "Finesse"
      },
      {
        name: "Light"
      },
      {
        name: "Thrown"
      },
      {
        name: "Monk"
      }
    ],
    throw_range: {
      normal: 20,
      long: 60
    }
  },
  {
    index: 3,
    name: "Greatclub",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.SimpleMelee,
    cost: {
      quantity: 2,
      unit: Unit.SP
    },
    damage: {
      dice_count: 1,
      dice_value: 8,
      damage_type: {
        name: "Bludgeoning"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 10,
    properties: [
      {
        name: "Two-Handed"
      }
    ]
  },
  {
    index: 4,
    name: "Handaxe",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.SimpleMelee,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 6,
      damage_type: {
        name: "Slashing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 2,
    properties: [
      {
        name: "Light"
      },
      {
        name: "Thrown"
      },
      {
        name: "Monk"
      }
    ],
    throw_range: {
      normal: 20,
      long: 60
    }
  },
  {
    index: 5,
    name: "Javelin",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.SimpleMelee,
    cost: {
      quantity: 5,
      unit: Unit.SP
    },
    damage: {
      dice_count: 1,
      dice_value: 6,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 2,
    properties: [
      {
        name: "Thrown"
      },
      {
        name: "Monk"
      }
    ],
    throw_range: {
      normal: 30,
      long: 120
    }
  },
  {
    index: 6,
    name: "Light hammer",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.SimpleMelee,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 4,
      damage_type: {
        name: "Bludgeoning"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 2,
    properties: [
      {
        name: "Light"
      },
      {
        name: "Thrown"
      },
      {
        name: "Monk"
      }
    ],
    throw_range: {
      normal: 20,
      long: 60
    }
  },
  {
    index: 7,
    name: "Mace",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.SimpleMelee,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 6,
      damage_type: {
        name: "Bludgeoning"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 4,
    properties: [
      {
        name: "Monk"
      }
    ]
  },
  {
    index: 8,
    name: "Quarterstaff",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.SimpleMelee,
    cost: {
      quantity: 2,
      unit: Unit.SP
    },
    damage: {
      dice_count: 1,
      dice_value: 6,
      damage_type: {
        name: "Bludgeoning"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 4,
    properties: [
      {
        name: "Versatile"
      },
      {
        name: "Monk"
      }
    ],
    "2h_damage": {
      dice_count: 1,
      dice_value: 8,
      damage_type: {
        name: "Bludgeoning"
      }
    }
  },
  {
    index: 9,
    name: "Sickle",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.SimpleMelee,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 4,
      damage_type: {
        name: "Slashing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 2,
    properties: [
      {
        name: "Light"
      },
      {
        name: "Monk"
      }
    ]
  },
  {
    index: 10,
    name: "Spear",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.SimpleMelee,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 4,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 3,
    properties: [
      {
        name: "Thrown"
      },
      {
        name: "Versatile"
      },
      {
        name: "Monk"
      }
    ],
    throw_range: {
      normal: 20,
      long: 60
    },
    "2h_damage": {
      dice_count: 1,
      dice_value: 8,
      damage_type: {
        name: "Piercing"
      }
    }
  },
  {
    index: 11,
    name: "Crossbow, light",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Ranged,
    category_range: CategoryRange.SimpleRanged,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 8,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 5,
    properties: [
      {
        name: "Ammunition"
      },
      {
        name: "Loading"
      },
      {
        name: "Two-Handed"
      }
    ]
  },
  {
    index: 12,
    name: "Dart",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Ranged,
    category_range: CategoryRange.SimpleRanged,
    cost: {
      quantity: 5,
      unit: Unit.Cp
    },
    damage: {
      dice_count: 1,
      dice_value: 4,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 0.25,
    properties: [
      {
        name: "Finesse"
      },
      {
        name: "Thrown"
      }
    ],
    throw_range: {
      normal: 20,
      long: 60
    }
  },
  {
    index: 13,
    name: "Shortbow",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Ranged,
    category_range: CategoryRange.SimpleRanged,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 6,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 2,
    properties: [
      {
        name: "Ammunition"
      },
      {
        name: "Two-Handed"
      }
    ]
  },
  {
    index: 14,
    name: "Sling",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Simple,
    weapon_range: WeaponRange.Ranged,
    category_range: CategoryRange.SimpleRanged,
    cost: {
      quantity: 1,
      unit: Unit.SP
    },
    damage: {
      dice_count: 1,
      dice_value: 4,
      damage_type: {
        name: "Bludgeoning"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 0,
    properties: [
      {
        name: "Ammunition"
      }
    ]
  },
  {
    index: 15,
    name: "Battleaxe",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 8,
      damage_type: {
        name: "Slashing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 4,
    properties: [
      {
        name: "Versatile"
      }
    ],
    "2h_damage": {
      dice_count: 1,
      dice_value: 10,
      damage_type: {
        name: "Slashing"
      }
    }
  },
  {
    index: 16,
    name: "Flail",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 8,
      damage_type: {
        name: "Bludgeoning"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 2,
    properties: []
  },
  {
    index: 17,
    name: "Glaive",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 20,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 10,
      damage_type: {
        name: "Slashing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 6,
    properties: [
      {
        name: "Heavy"
      },
      {
        name: "Reach"
      },
      {
        name: "Two-Handed"
      }
    ]
  },
  {
    index: 18,
    name: "Greataxe",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 30,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 12,
      damage_type: {
        name: "Slashing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 7,
    properties: [
      {
        name: "Heavy"
      },
      {
        name: "Two-Handed"
      }
    ]
  },
  {
    index: 19,
    name: "Greatsword",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 2,
      dice_value: 6,
      damage_type: {
        name: "Slashing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 6,
    properties: [
      {
        name: "Heavy"
      },
      {
        name: "Two-Handed"
      }
    ]
  },
  {
    index: 20,
    name: "Halberd",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 20,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 10,
      damage_type: {
        name: "Slashing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 6,
    properties: [
      {
        name: "Heavy"
      },
      {
        name: "Reach"
      },
      {
        name: "Two-Handed"
      }
    ]
  },
  {
    index: 21,
    name: "Lance",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 12,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 6,
    properties: [
      {
        name: "Reach"
      },
      {
        name: "Special"
      }
    ],
    special: [
      "You have disadvantage when you use a lance to attack a target within 5 feet of you. Also, a lance requires two hands to wield when you aren’t mounted."
    ]
  },
  {
    index: 22,
    name: "Longsword",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 15,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 8,
      damage_type: {
        name: "Slashing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 3,
    properties: [
      {
        name: "Versatile"
      }
    ],
    "2h_damage": {
      dice_count: 1,
      dice_value: 10,
      damage_type: {
        name: "Slashing"
      }
    }
  },
  {
    index: 23,
    name: "Maul",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 2,
      dice_value: 6,
      damage_type: {
        name: "Bludgeoning"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 10,
    properties: [
      {
        name: "Heavy"
      },
      {
        name: "Two-Handed"
      }
    ]
  },
  {
    index: 24,
    name: "Morningstar",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 15,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 8,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 4,
    properties: []
  },
  {
    index: 25,
    name: "Pike",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 10,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 18,
    properties: [
      {
        name: "Heavy"
      },
      {
        name: "Reach"
      },
      {
        name: "Two-Handed"
      }
    ]
  },
  {
    index: 26,
    name: "Rapier",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 8,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 2,
    properties: [
      {
        name: "Finesse"
      }
    ]
  },
  {
    index: 27,
    name: "Scimitar",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 6,
      damage_type: {
        name: "Slashing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 3,
    properties: [
      {
        name: "Finesse"
      },
      {
        name: "Light"
      }
    ]
  },
  {
    index: 28,
    name: "Shortsword",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 6,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 2,
    properties: [
      {
        name: "Finesse"
      },
      {
        name: "Light"
      },
      {
        name: "Monk"
      }
    ]
  },
  {
    index: 29,
    name: "Trident",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 6,
      damage_type: {
        name: "Slashing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 4,
    properties: [
      {
        name: "Thrown"
      },
      {
        name: "Versatile"
      }
    ],
    throw_range: {
      normal: 20,
      long: 60
    }
  },
  {
    index: 30,
    name: "War pick",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 8,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 2,
    properties: []
  },
  {
    index: 31,
    name: "Warhammer",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 15,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 8,
      damage_type: {
        name: "Bludgeoning"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 2,
    properties: [
      {
        name: "Versatile"
      }
    ],
    "2h_damage": {
      dice_count: 1,
      dice_value: 10,
      damage_type: {
        name: "Bludgeoning"
      }
    }
  },
  {
    index: 32,
    name: "Whip",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Melee,
    category_range: CategoryRange.MartialMelee,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 4,
      damage_type: {
        name: "Slashing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 3,
    properties: [
      {
        name: "Finesse"
      },
      {
        name: "Reach"
      }
    ]
  },
  {
    index: 33,
    name: "Blowgun",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Ranged,
    category_range: CategoryRange.MartialRanged,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 1,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 1,
    properties: [
      {
        name: "Ammunition"
      },
      {
        name: "Loading"
      }
    ]
  },
  {
    index: 34,
    name: "Crossbow, hand",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Ranged,
    category_range: CategoryRange.MartialRanged,
    cost: {
      quantity: 75,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 6,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 3,
    properties: [
      {
        name: "Ammunition"
      },
      {
        name: "Light"
      },
      {
        name: "Loading"
      }
    ]
  },
  {
    index: 35,
    name: "Crossbow, heavy",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Ranged,
    category_range: CategoryRange.MartialRanged,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 10,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 18,
    properties: [
      {
        name: "Ammunition"
      },
      {
        name: "Light"
      },
      {
        name: "Loading"
      },
      {
        name: "Two-Handed"
      }
    ]
  },
  {
    index: 36,
    name: "Longbow",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Ranged,
    category_range: CategoryRange.MartialRanged,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 8,
      damage_type: {
        name: "Piercing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 2,
    properties: [
      {
        name: "Ammunition"
      },
      {
        name: "Heavy"
      },
      {
        name: "Two-Handed"
      }
    ]
  },
  {
    index: 37,
    name: "Net",
    equipment_category: EquipmentCategory.Weapon,
    weapon_category: WeaponCategory.Martial,
    weapon_range: WeaponRange.Ranged,
    category_range: CategoryRange.MartialRanged,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    damage: {
      dice_count: 1,
      dice_value: 0,
      damage_type: {
        name: "Slashing"
      }
    },
    range: {
      normal: 5,
      long: null
    },
    weight: 3,
    properties: [
      {
        name: "Thrown"
      },
      {
        name: "Special"
      }
    ],
    special: [
      "A Large or smaller creature hit by a net is restrained until it is freed. A net has no effect on creatures that are formless, or creatures that are Huge or larger. A creature can use its action to make a DC 10 Strength check, freeing itself or another creature within its reach on a success. Dealing 5 slashing damage to the net (AC 10) also frees the creature without harming it, ending the effect and destroying the net. When you use an action, bonus action, or reaction to attack with a net, you can make only one attack regardless of the number of attacks you can normally make."
    ],
    throw_range: {
      normal: 5,
      long: 15
    }
  },
  {
    index: 38,
    name: "Padded",
    equipment_category: EquipmentCategory.Armor,
    armor_category: "Light",
    armor_class: {
      base: 11,
      dex_bonus: true,
      max_bonus: null
    },
    str_minimum: 0,
    stealth_disadvantage: true,
    weight: 8,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    }
  },
  {
    index: 39,
    name: "Leather",
    equipment_category: EquipmentCategory.Armor,
    armor_category: "Light",
    armor_class: {
      base: 11,
      dex_bonus: true,
      max_bonus: null
    },
    str_minimum: 0,
    stealth_disadvantage: false,
    weight: 10,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    }
  },
  {
    index: 40,
    name: "Studded Leather",
    equipment_category: EquipmentCategory.Armor,
    armor_category: "Light",
    armor_class: {
      base: 12,
      dex_bonus: true,
      max_bonus: null
    },
    str_minimum: 0,
    stealth_disadvantage: false,
    weight: 13,
    cost: {
      quantity: 45,
      unit: Unit.Gp
    }
  },
  {
    index: 41,
    name: "Hide",
    equipment_category: EquipmentCategory.Armor,
    armor_category: "Medium",
    armor_class: {
      base: 12,
      dex_bonus: true,
      max_bonus: 2
    },
    str_minimum: 0,
    stealth_disadvantage: false,
    weight: 12,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    }
  },
  {
    index: 42,
    name: "Chain Shirt",
    equipment_category: EquipmentCategory.Armor,
    armor_category: "Medium",
    armor_class: {
      base: 13,
      dex_bonus: true,
      max_bonus: 2
    },
    str_minimum: 0,
    stealth_disadvantage: false,
    weight: 20,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    }
  },
  {
    index: 43,
    name: "Scale Mail",
    equipment_category: EquipmentCategory.Armor,
    armor_category: "Medium",
    armor_class: {
      base: 14,
      dex_bonus: true,
      max_bonus: 2
    },
    str_minimum: 0,
    stealth_disadvantage: true,
    weight: 45,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    }
  },
  {
    index: 44,
    name: "Breastplate",
    equipment_category: EquipmentCategory.Armor,
    armor_category: "Medium",
    armor_class: {
      base: 14,
      dex_bonus: true,
      max_bonus: 2
    },
    str_minimum: 0,
    stealth_disadvantage: false,
    weight: 20,
    cost: {
      quantity: 400,
      unit: Unit.Gp
    }
  },
  {
    index: 45,
    name: "Half Plate",
    equipment_category: EquipmentCategory.Armor,
    armor_category: "Medium",
    armor_class: {
      base: 15,
      dex_bonus: true,
      max_bonus: 2
    },
    str_minimum: 0,
    stealth_disadvantage: true,
    weight: 40,
    cost: {
      quantity: 750,
      unit: Unit.Gp
    }
  },
  {
    index: 46,
    name: "Ring Mail",
    equipment_category: EquipmentCategory.Armor,
    armor_category: "Heavy",
    armor_class: {
      base: 14,
      dex_bonus: false,
      max_bonus: null
    },
    str_minimum: 0,
    stealth_disadvantage: true,
    weight: 40,
    cost: {
      quantity: 30,
      unit: Unit.Gp
    }
  },
  {
    index: 47,
    name: "Chain Mail",
    equipment_category: EquipmentCategory.Armor,
    armor_category: "Heavy",
    armor_class: {
      base: 16,
      dex_bonus: false,
      max_bonus: null
    },
    str_minimum: 13,
    stealth_disadvantage: true,
    weight: 55,
    cost: {
      quantity: 75,
      unit: Unit.Gp
    }
  },
  {
    index: 48,
    name: "Splint",
    equipment_category: EquipmentCategory.Armor,
    armor_category: "Heavy",
    armor_class: {
      base: 17,
      dex_bonus: false,
      max_bonus: null
    },
    str_minimum: 15,
    stealth_disadvantage: true,
    weight: 60,
    cost: {
      quantity: 200,
      unit: Unit.Gp
    }
  },
  {
    index: 49,
    name: "Plate",
    equipment_category: EquipmentCategory.Armor,
    armor_category: "Heavy",
    armor_class: {
      base: 18,
      dex_bonus: false,
      max_bonus: null
    },
    str_minimum: 15,
    stealth_disadvantage: true,
    weight: 65,
    cost: {
      quantity: 1500,
      unit: Unit.Gp
    }
  },
  {
    index: 50,
    name: "Shield",
    equipment_category: EquipmentCategory.Armor,
    armor_category: "Shield",
    armor_class: {
      base: 2,
      dex_bonus: false,
      max_bonus: null
    },
    str_minimum: 0,
    stealth_disadvantage: false,
    weight: 6,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    }
  },
  {
    index: 51,
    name: "Abacus",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 2
  },
  {
    index: 52,
    name: "Acid (vial)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    weight: 1,
    desc: [
      "As an action, you can splash the contents of this vial onto a creature within 5 feet of you or throw the vial up to 20 feet, shattering it on impact. In either case, make a ranged attack against a creature or object, treating the acid as an improvised weapon.",
      "On a hit, the target takes 2d6 acid damage."
    ]
  },
  {
    index: 53,
    name: "Alchemist’s fire (flask)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    },
    desc: [
      "This sticky, adhesive fluid ignites when exposed to air.",
      "As an action, you can throw this flask up to 20 feet, shattering it on impact. Make a ranged attack against a creature or object, treating the alchemist’s fire as an improvised weapon.",
      "On a hit, the target takes 1d4 fire damage at the start of each of its turns. A creature can end this damage by using its action to make a DC 10 Dexterity check to extinguish the flames."
    ],
    weight: 1
  },
  {
    index: 54,
    name: "Arrow",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.Ammunition,
    cost: {
      quantity: 5,
      unit: Unit.Cp
    },
    weight: 1
  },
  {
    index: 55,
    name: "Blowgun needle",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.Ammunition,
    cost: {
      quantity: 2,
      unit: Unit.Cp
    },
    weight: 1
  },
  {
    index: 56,
    name: "Crossbow bolt",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.Ammunition,
    cost: {
      quantity: 5,
      unit: Unit.Cp
    },
    weight: 1.5
  },
  {
    index: 57,
    name: "Sling bullet",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.Ammunition,
    cost: {
      quantity: 1,
      unit: Unit.Cp
    },
    weight: 1.5
  },
  {
    index: 58,
    name: "Amulet",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.HolySymbol,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 1,
    desc: [
      "A holy symbol is a representation of a god or pantheon. It might be an amulet depicting a symbol representing a deity, the same symbol carefully engraved or inlaid as an emblem on a shield, or a tiny box holding a fragment of a sacred relic.",
      "Appendix B lists the symbols commonly associated with many gods in the multiverse. A cleric or paladin can use a holy symbol as a spellcasting focus. To use the symbol in this way, the caster must hold it in hand, wear it visibly, or bear it on a shield."
    ]
  },
  {
    index: 59,
    name: "Antitoxin (vial)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    },
    weight: 0,
    desc: [
      "A creature that drinks this vial of liquid gains advantage on saving throws against poison for 1 hour. It confers no benefit to undead or constructs."
    ]
  },
  {
    index: 60,
    name: "Crystal",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.ArcaneFocus,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 1,
    desc: [
      "An arcane focus is a special item— an orb, a crystal, a rod, a specially constructed staff, a wand-like length of wood, or some similar item— designed to channel the power of arcane spells. A sorcerer, warlock, or wizard can use such an item as a spellcasting focus."
    ]
  },
  {
    index: 61,
    name: "Orb",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.ArcaneFocus,
    cost: {
      quantity: 20,
      unit: Unit.Gp
    },
    weight: 3,
    desc: [
      "An arcane focus is a special item— an orb, a crystal, a rod, a specially constructed staff, a wand-like length of wood, or some similar item— designed to channel the power of arcane spells. A sorcerer, warlock, or wizard can use such an item as a spellcasting focus."
    ]
  },
  {
    index: 62,
    name: "Rod",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.ArcaneFocus,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 2,
    desc: [
      "An arcane focus is a special item— an orb, a crystal, a rod, a specially constructed staff, a wand-like length of wood, or some similar item— designed to channel the power of arcane spells. A sorcerer, warlock, or wizard can use such an item as a spellcasting focus."
    ]
  },
  {
    index: 63,
    name: "Staff",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.ArcaneFocus,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 4,
    desc: [
      "An arcane focus is a special item— an orb, a crystal, a rod, a specially constructed staff, a wand-like length of wood, or some similar item— designed to channel the power of arcane spells. A sorcerer, warlock, or wizard can use such an item as a spellcasting focus."
    ]
  },
  {
    index: 64,
    name: "Wand",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.ArcaneFocus,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 1,
    desc: [
      "An arcane focus is a special item— an orb, a crystal, a rod, a specially constructed staff, a wand-like length of wood, or some similar item— designed to channel the power of arcane spells. A sorcerer, warlock, or wizard can use such an item as a spellcasting focus."
    ]
  },
  {
    index: 65,
    name: "Backpack",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 5
  },
  {
    index: 66,
    name: "Ball bearings (bag of 1,000)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 2,
    desc: [
      "As an action, you can spill these tiny metal balls from their pouch to cover a level, square area that is 10 feet on a side.",
      "A creature moving across the covered area must succeed on a DC 10 Dexterity saving throw or fall prone.",
      "A creature moving through the area at half speed doesn’t need to make the save."
    ]
  },
  {
    index: 67,
    name: "Barrel",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 70
  },
  {
    index: 68,
    name: "Basket",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 4,
      unit: Unit.SP
    },
    weight: 2
  },
  {
    index: 69,
    name: "Bedroll",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 7
  },
  {
    index: 70,
    name: "Bell",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 0
  },
  {
    index: 71,
    name: "Blanket",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.SP
    },
    weight: 3
  },
  {
    index: 72,
    name: "Block and tackle",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 5,
    desc: [
      "A set of pulleys with a cable threaded through them and a hook to attach to objects, a block and tackle allows you to hoist up to four times the weight you can normally lift."
    ]
  },
  {
    index: 73,
    name: "Book",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    weight: 5,
    desc: [
      "A book might contain poetry, historical accounts, information pertaining to a particular field of lore, diagrams and notes on gnomish contraptions, or just about anything else that can be represented using text or pictures. A book of spells is a spellbook (described later in this section)."
    ]
  },
  {
    index: 74,
    name: "Bottle, glass",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 2
  },
  {
    index: 75,
    name: "Bucket",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Cp
    },
    weight: 2
  },
  {
    index: 76,
    name: "Caltrops",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Cp
    },
    weight: 2,
    desc: [
      "As an action, you can spread a bag of caltrops to cover a square area that is 5 feet on a side.",
      "Any creature that enters the area must succeed on a DC 15 Dexterity saving throw or stop moving this turn and take 1 piercing damage.",
      "Taking this damage reduces the creature’s walking speed by 10 feet until the creature regains at least 1 hit point.",
      "A creature moving through the area at half speed doesn’t need to make the save."
    ]
  },
  {
    index: 77,
    name: "Candle",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Cp
    },
    weight: 0,
    desc: [
      "For 1 hour, a candle sheds bright light in a 5-foot radius and dim light for an additional 5 feet."
    ]
  },
  {
    index: 78,
    name: "Case, crossbow bolt",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 1,
    desc: ["This wooden case can hold up to twenty crossbow bolts."]
  },
  {
    index: 79,
    name: "Case, map or scroll",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 1,
    desc: [
      "This cylindrical leather case can hold up to ten rolled-up sheets of paper or five rolled-up sheets of parchment."
    ]
  },
  {
    index: 80,
    name: "Chain (10 feet)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 10,
    desc: ["A chain has 10 hit points. It can be burst with a successful DC 20 Strength check."]
  },
  {
    index: 81,
    name: "Chalk (1 piece)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Cp
    },
    weight: 0
  },
  {
    index: 82,
    name: "Chest",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 25
  },
  {
    index: 83,
    name: "Clothes, common",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.SP
    },
    weight: 3
  },
  {
    index: 84,
    name: "Clothes, costume",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 4
  },
  {
    index: 85,
    name: "Clothes, fine",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 15,
      unit: Unit.Gp
    },
    weight: 6
  },
  {
    index: 86,
    name: "Clothes, traveler’s",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 4
  },
  {
    index: 87,
    name: "Component pouch",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    weight: 2,
    desc: [
      " A component pouch is a small, watertight leather belt pouch that has compartments to hold all the material components and other special items you need to cast your spells, except for those components that have a specific cost (as indicated in a spell’s description)."
    ]
  },
  {
    index: 88,
    name: "Crowbar",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 5,
    desc: [
      "Using a crowbar grants advantage to Strength checks where the crowbar’s leverage can be applied."
    ]
  },
  {
    index: 89,
    name: "Sprig of mistletoe",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.DruidicFocus,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 0,
    desc: [
      "A druidic focus might be a sprig of mistletoe or holly, a wand or scepter made of yew or another special wood, a staff drawn whole out of a living tree, or a totem object incorporating feathers, fur, bones, and teeth from sacred animals. A druid can use such an object as a spellcasting focus."
    ]
  },
  {
    index: 90,
    name: "Totem",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.DruidicFocus,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 0,
    desc: [
      "A druidic focus might be a sprig of mistletoe or holly, a wand or scepter made of yew or another special wood, a staff drawn whole out of a living tree, or a totem object incorporating feathers, fur, bones, and teeth from sacred animals. A druid can use such an object as a spellcasting focus."
    ]
  },
  {
    index: 91,
    name: "Wooden staff",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.DruidicFocus,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 4,
    desc: [
      "A druidic focus might be a sprig of mistletoe or holly, a wand or scepter made of yew or another special wood, a staff drawn whole out of a living tree, or a totem object incorporating feathers, fur, bones, and teeth from sacred animals. A druid can use such an object as a spellcasting focus."
    ]
  },
  {
    index: 92,
    name: "Yew wand",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.DruidicFocus,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 1,
    desc: [
      "A druidic focus might be a sprig of mistletoe or holly, a wand or scepter made of yew or another special wood, a staff drawn whole out of a living tree, or a totem object incorporating feathers, fur, bones, and teeth from sacred animals. A druid can use such an object as a spellcasting focus."
    ]
  },
  {
    index: 93,
    name: "Emblem",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.HolySymbol,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 0,
    desc: [
      "A holy symbol is a representation of a god or pantheon. It might be an amulet depicting a symbol representing a deity, the same symbol carefully engraved or inlaid as an emblem on a shield, or a tiny box holding a fragment of a sacred relic.",
      "Appendix B lists the symbols commonly associated with many gods in the multiverse. A cleric or paladin can use a holy symbol as a spellcasting focus. To use the symbol in this way, the caster must hold it in hand, wear it visibly, or bear it on a shield."
    ]
  },
  {
    index: 94,
    name: "Fishing tackle",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 4,
    desc: [
      "This kit includes a wooden rod, silken line, corkwood bobbers, steel hooks, lead sinkers, velvet lures, and narrow netting."
    ]
  },
  {
    index: 95,
    name: "Flask or tankard",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Cp
    },
    weight: 1
  },
  {
    index: 96,
    name: "Grappling hook",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 4
  },
  {
    index: 97,
    name: "Hammer",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 3
  },
  {
    index: 98,
    name: "Hammer, sledge",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 10
  },
  {
    index: 99,
    name: "Holy water (flask)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    weight: 1,
    desc: [
      "As an action, you can splash the contents of this flask onto a creature within 5 feet of you or throw it up to 20 feet, shattering it on impact. In either case, make a ranged attack against a target creature, treating the holy water as an improvised weapon.",
      "If the target is a fiend or undead, it takes 2d6 radiant damage.",
      "A cleric or paladin may create holy water by performing a special ritual.",
      "The ritual takes 1 hour to perform, uses 25 gp worth of powdered silver, and requires the caster to expend a 1st-level spell slot."
    ]
  },
  {
    index: 100,
    name: "Hourglass",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    weight: 1
  },
  {
    index: 101,
    name: "Hunting trap",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    desc: [
      "When you use your action to set it, this trap forms a saw-toothed steel ring that snaps shut when a creature steps on a pressure plate in the center. The trap is affixed by a heavy chain to an immobile object, such as a tree or a spike driven into the ground.",
      "A creature that steps on the plate must succeed on a DC 13 Dexterity saving throw or take 1d4 piercing damage and stop moving. Thereafter, until the creature breaks free of the trap, its movement is limited by the length of the chain (typically 3 feet long).",
      "A creature can use its action to make a DC 13 Strength check, freeing itself or another creature within its reach on a success. Each failed check deals 1 piercing damage to the trapped creature."
    ],
    weight: 25
  },
  {
    index: 102,
    name: "Ink (1 ounce bottle)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 0
  },
  {
    index: 103,
    name: "Ink pen",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Cp
    },
    weight: 0
  },
  {
    index: 104,
    name: "Jug or pitcher",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Cp
    },
    weight: 4
  },
  {
    index: 105,
    name: "Climber’s Kit",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.Kit,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    weight: 12,
    desc: [
      "A climber’s kit includes special pitons, boot tips, gloves, and a harness. You can use the climber’s kit as an action to anchor yourself; when you do, you can’t fall more than 25 feet from the point where you anchored yourself, and you can’t climb more than 25 feet away from that point without undoing the anchor."
    ]
  },
  {
    index: 106,
    name: "Disguise Kit",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.Kit,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    weight: 3,
    desc: [
      "This pouch of cosmetics, hair dye, and small props lets you create disguises that change your physical appearance. Proficiency with this kit lets you add your proficiency bonus to any ability checks you make to create a visual disguise."
    ]
  },
  {
    index: 107,
    name: "Forgery Kit",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.Kit,
    cost: {
      quantity: 15,
      unit: Unit.Gp
    },
    weight: 5,
    desc: [
      "This small box contains a variety of papers and parchments, pens and inks, seals and sealing wax, gold and silver leaf, and other supplies necessary to create convincing forgeries of physical documents. Proficiency with this kit lets you add your proficiency bonus to any ability checks you make to create a physical forgery of a document."
    ]
  },
  {
    index: 108,
    name: "Herbalism Kit",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.Kit,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 3,
    desc: [
      "This kit contains a variety of instruments such as clippers, mortar and pestle, and pouches and vials used by herbalists to create remedies and potions. Proficiency with this kit lets you add your proficiency bonus to any ability checks you make to identify or apply herbs. Also, proficiency with this kit is required to create antitoxin and potions of healing."
    ]
  },
  {
    index: 109,
    name: "Healer’s Kit",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.Kit,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 3,
    desc: [
      "This kit is a leather pouch containing bandages, salves, and splints. The kit has ten uses. As an action, you can expend one use of the kit to stabilize a creature that has 0 hit points, without needing to make a Wisdom (Medicine) check."
    ]
  },
  {
    index: 110,
    name: "Mess Kit",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.Kit,
    cost: {
      quantity: 2,
      unit: Unit.SP
    },
    weight: 1,
    desc: [
      "This tin box contains a cup and simple cutlery. The box clamps together, and one side can be used as a cooking pan and the other as a plate or shallow bowl."
    ]
  },
  {
    index: 111,
    name: "Poisoner’s Kit",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.Kit,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    },
    weight: 2,
    desc: [
      "A poisoner’s kit includes the vials, chemicals, and other equipment necessary for the creation of poisons. Proficiency with this kit lets you add your proficiency bonus to any ability checks you make to craft or use poisons."
    ]
  },
  {
    index: 112,
    name: "Ladder (10-foot)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.SP
    },
    weight: 25
  },
  {
    index: 113,
    name: "Lamp",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.SP
    },
    weight: 1,
    desc: [
      "A lamp casts bright light in a 15-foot radius and dim light for an additional 30 feet. Once lit, it burns for 6 hours on a flask (1 pint) of oil."
    ]
  },
  {
    index: 114,
    name: "Lantern, bullseye",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 2,
    desc: [
      "A bullseye lantern casts bright light in a 60-foot cone and dim light for an additional 60 feet. Once lit, it burns for 6 hours on a flask (1 pint) of oil."
    ]
  },
  {
    index: 115,
    name: "Lantern, hooded",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 2,
    desc: [
      "A hooded lantern casts bright light in a 30-foot radius and dim light for an additional 30 feet. Once lit, it burns for 6 hours on a flask (1 pint) of oil. As an action, you can lower the hood, reducing the light to dim light in a 5-foot radius."
    ]
  },
  {
    index: 116,
    name: "Lock",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 1,
    desc: [
      "A key is provided with the lock. Without the key, a creature proficient with thieves’ tools can pick this lock with a successful DC 15 Dexterity check. Your GM may decide that better locks are available for higher prices."
    ]
  },
  {
    index: 117,
    name: "Magnifying glass",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 100,
      unit: Unit.Gp
    },
    weight: 0,
    desc: [
      "This lens allows a closer look at small objects. It is also useful as a substitute for flint and steel when starting fires. Lighting a fire with a magnifying glass requires light as bright as sunlight to focus, tinder to ignite, and about 5 minutes for the fire to ignite.",
      "A magnifying glass grants advantage on any ability check made to appraise or inspect an item that is small or highly detailed."
    ]
  },
  {
    index: 118,
    name: "Manacles",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 6,
    desc: [
      "These metal restraints can bind a Small or Medium creature. Escaping the manacles requires a successful DC 20 Dexterity check. Breaking them requires a successful DC 20 Strength check.",
      "Each set of manacles comes with one key. Without the key, a creature proficient with thieves’ tools can pick the manacles’ lock with a successful DC 15 Dexterity check. Manacles have 15 hit points."
    ]
  },
  {
    index: 119,
    name: "Mirror, steel",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 0.5
  },
  {
    index: 120,
    name: "Oil (flask)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.SP
    },
    weight: 1,
    desc: [
      "Oil usually comes in a clay flask that holds 1 pint.",
      "As an action, you can splash the oil in this flask onto a creature within 5 feet of you or throw it up to 20 feet, shattering it on impact. Make a ranged attack against a target creature or object, treating the oil as an improvised weapon.",
      "On a hit, the target is covered in oil. If the target takes any fire damage before the oil dries (after 1 minute), the target takes an additional 5 fire damage from the burning oil.",
      "You can also pour a flask of oil on the ground to cover a 5-foot-square area, provided that the surface is level.",
      "If lit, the oil burns for 2 rounds and deals 5 fire damage to any creature that enters the area or ends its turn in the area. A creature can take this damage only once per turn."
    ]
  },
  {
    index: 121,
    name: "Paper (one sheet)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.SP
    },
    weight: 0
  },
  {
    index: 122,
    name: "Parchment (one sheet)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.SP
    },
    weight: 0
  },
  {
    index: 123,
    name: "Perfume (vial)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 0
  },
  {
    index: 124,
    name: "Pick, miner’s",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 10
  },
  {
    index: 125,
    name: "Piton",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Cp
    },
    weight: 0.25
  },
  {
    index: 126,
    name: "Poison, basic (vial)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 100,
      unit: Unit.Gp
    },
    weight: 0,
    desc: [
      "You can use the poison in this vial to coat one slashing or piercing weapon or up to three pieces of ammunition. Applying the poison takes an action. A creature hit by the poisoned weapon or ammunition must make a DC 10 Constitution saving throw or take 1d4 poison damage. Once applied, the poison retains potency for 1 minute before drying."
    ]
  },
  {
    index: 127,
    name: "Pole (10-foot)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Cp
    },
    weight: 7
  },
  {
    index: 128,
    name: "Pot, iron",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 10
  },
  {
    index: 129,
    name: "Potion of healing",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    },
    weight: 0.5,
    desc: [
      "A character who drinks the magical red fluid in this vial regains 2d4 + 2 hit points. Drinking or administering a potion takes an action."
    ]
  },
  {
    index: 130,
    name: "Pouch",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.SP
    },
    weight: 1,
    desc: [
      "A cloth or leather pouch can hold up to 20 sling bullets or 50 blowgun needles, among other things. A compartmentalized pouch for holding spell components is called a component pouch (described earlier in this section)."
    ]
  },
  {
    index: 131,
    name: "Quiver",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 1,
    desc: ["A quiver can hold up to 20 arrows."]
  },
  {
    index: 132,
    name: "Ram, portable",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 4,
      unit: Unit.Gp
    },
    weight: 35,
    desc: [
      "You can use a portable ram to break down doors. When doing so, you gain a +4 bonus on the Strength check. One other character can help you use the ram, giving you advantage on this check."
    ]
  },
  {
    index: 133,
    name: "Rations (1 day)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.SP
    },
    weight: 2,
    desc: [
      "Rations consist of dry foods suitable for extended travel, including jerky, dried fruit, hardtack, and nuts."
    ]
  },
  {
    index: 134,
    name: "Reliquary",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.HolySymbol,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 2,
    desc: [
      "A holy symbol is a representation of a god or pantheon. It might be an amulet depicting a symbol representing a deity, the same symbol carefully engraved or inlaid as an emblem on a shield, or a tiny box holding a fragment of a sacred relic.",
      "Appendix B lists the symbols commonly associated with many gods in the multiverse. A cleric or paladin can use a holy symbol as a spellcasting focus. To use the symbol in this way, the caster must hold it in hand, wear it visibly, or bear it on a shield."
    ]
  },
  {
    index: 135,
    name: "Robes",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 4
  },
  {
    index: 136,
    name: "Rope, hempen (50 feet)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 10,
    desc: [
      "Rope, whether made of hemp or silk, has 2 hit points and can be burst with a DC 17 Strength check."
    ]
  },
  {
    index: 137,
    name: "Rope, silk (50 feet)",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 5,
    desc: [
      "Rope, whether made of hemp or silk, has 2 hit points and can be burst with a DC 17 Strength check."
    ]
  },
  {
    index: 138,
    name: "Sack",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Cp
    },
    weight: 0.5
  },
  {
    index: 139,
    name: "Scale, merchant’s",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 3,
    desc: [
      "A scale includes a small balance, pans, and a suitable assortment of weights up to 2 pounds. With it, you can measure the exact weight of small objects, such as raw precious metals or trade goods, to help determine their worth."
    ]
  },
  {
    index: 140,
    name: "Sealing wax",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.SP
    },
    weight: 0
  },
  {
    index: 141,
    name: "Shovel",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 5
  },
  {
    index: 142,
    name: "Signal whistle",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Cp
    },
    weight: 0
  },
  {
    index: 143,
    name: "Signet ring",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 0
  },
  {
    index: 144,
    name: "Soap",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Cp
    },
    weight: 0
  },
  {
    index: 145,
    name: "Spellbook",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    },
    weight: 3,
    desc: [
      "Essential for wizards, a spellbook is a leather-bound tome with 100 blank vellum pages suitable for recording spells."
    ]
  },
  {
    index: 146,
    name: "Spike, iron",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.SP
    },
    weight: 5
  },
  {
    index: 147,
    name: "Spyglass",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1000,
      unit: Unit.Gp
    },
    weight: 1,
    desc: ["Objects viewed through a spyglass are magnified to twice their size."]
  },
  {
    index: 148,
    name: "Tent, two-person",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 20,
    desc: ["A simple and portable canvas shelter, a tent sleeps two."]
  },
  {
    index: 149,
    name: "Tinderbox",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 5,
      unit: Unit.SP
    },
    weight: 1,
    desc: [
      "This small container holds flint, fire steel, and tinder (usually dry cloth soaked in light oil) used to kindle a fire. Using it to light a torch—or anything else with abundant, exposed fuel—takes an action.",
      "Lighting any other fire takes 1 minute."
    ]
  },
  {
    index: 150,
    name: "Torch",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Cp
    },
    weight: 1,
    desc: [
      "A torch burns for 1 hour, providing bright light in a 20-foot radius and dim light for an additional 20 feet. If you make a melee attack with a burning torch and hit, it deals 1 fire damage."
    ]
  },
  {
    index: 151,
    name: "Vial",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 0
  },
  {
    index: 152,
    name: "Waterskin",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 2,
      unit: Unit.SP
    },
    weight: 5
  },
  {
    index: 153,
    name: "Whetstone",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.StandardGear,
    cost: {
      quantity: 1,
      unit: Unit.Cp
    },
    weight: 1
  },
  {
    index: 154,
    name: "Burglar's Pack",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.EquipmentPack,
    cost: {
      quantity: 16,
      unit: Unit.Gp
    },
    contents: [
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 5
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 10
      },
      {
        quantity: 1
      },
      {
        quantity: 2
      },
      {
        quantity: 5
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      }
    ]
  },
  {
    index: 155,
    name: "Diplomat's Pack",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.EquipmentPack,
    cost: {
      quantity: 39,
      unit: Unit.Gp
    },
    contents: [
      {
        quantity: 1
      },
      {
        quantity: 2
      },
      {
        quantity: 5
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 2
      },
      {
        quantity: 5
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      }
    ]
  },
  {
    index: 156,
    name: "Dungeoneer's Pack",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.EquipmentPack,
    cost: {
      quantity: 12,
      unit: Unit.Gp
    },
    contents: [
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 10
      },
      {
        quantity: 10
      },
      {
        quantity: 10
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      }
    ]
  },
  {
    index: 157,
    name: "Entertainer's Pack",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.EquipmentPack,
    cost: {
      quantity: 40,
      unit: Unit.Gp
    },
    contents: [
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 2
      },
      {
        quantity: 5
      },
      {
        quantity: 5
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      }
    ]
  },
  {
    index: 158,
    name: "Explorer's Pack",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.EquipmentPack,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    contents: [
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 10
      },
      {
        quantity: 10
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      }
    ]
  },
  {
    index: 159,
    name: "Priest's Pack",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.EquipmentPack,
    cost: {
      quantity: 19,
      unit: Unit.Gp
    },
    contents: [
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 10
      },
      {
        quantity: 1
      },
      {
        quantity: 2
      },
      {
        quantity: 1
      }
    ]
  },
  {
    index: 160,
    name: "Scholar's Pack",
    equipment_category: EquipmentCategory.AdventuringGear,
    gear_category: GearCategory.EquipmentPack,
    cost: {
      quantity: 40,
      unit: Unit.Gp
    },
    contents: [
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 1
      },
      {
        quantity: 10
      }
    ]
  },
  {
    index: 161,
    name: "Alchemist’s supplies",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    },
    weight: 8,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 162,
    name: "Brewer’s supplies",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 20,
      unit: Unit.Gp
    },
    weight: 9,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 163,
    name: "Calligrapher’s supplies",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 5,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 164,
    name: "Carpenter’s tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 8,
      unit: Unit.Gp
    },
    weight: 6,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 165,
    name: "Cartographer’s tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 15,
      unit: Unit.Gp
    },
    weight: 6,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 166,
    name: "Cobbler’s tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 5,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 167,
    name: "Cook’s utensils",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 8,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 168,
    name: "Glassblower’s tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 30,
      unit: Unit.Gp
    },
    weight: 5,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 169,
    name: "Jeweler’s tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    weight: 2,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 170,
    name: "Leatherworker’s tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 5,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 171,
    name: "Mason’s tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 8,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 172,
    name: "Painter’s supplies",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 5,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 173,
    name: "Potter’s tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 3,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 174,
    name: "Smith’s tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 20,
      unit: Unit.Gp
    },
    weight: 8,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 175,
    name: "Tinker’s tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    },
    weight: 10,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 176,
    name: "Weaver’s tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 5,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 177,
    name: "Woodcarver’s tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.ArtisanSTools,
    cost: {
      quantity: 1,
      unit: Unit.Gp
    },
    weight: 5,
    desc: [
      "These special tools include the items needed to pursue a craft or trade. The table shows examples of the most common types of tools, each providing items related to a single craft. Proficiency with a set of artisan’s tools lets you add your proficiency bonus to any ability checks you make using the tools in your craft. Each type of artisan’s tools requires a separate proficiency."
    ]
  },
  {
    index: 178,
    name: "Dice set",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.GamingSets,
    cost: {
      quantity: 1,
      unit: Unit.SP
    },
    weight: 0,
    desc: [
      "This item encompasses a wide range of game pieces, including dice and decks of cards (for games such as Three-Dragon Ante). A few common examples appear on the Tools table, but other kinds of gaming sets exist. If you are proficient with a gaming set, you can add your proficiency bonus to ability checks you make to play a game with that set. Each type of gaming set requires a separate proficiency."
    ]
  },
  {
    index: 179,
    name: "Playing card set",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.GamingSets,
    cost: {
      quantity: 5,
      unit: Unit.SP
    },
    weight: 0,
    desc: [
      "This item encompasses a wide range of game pieces, including dice and decks of cards (for games such as Three-Dragon Ante). A few common examples appear on the Tools table, but other kinds of gaming sets exist. If you are proficient with a gaming set, you can add your proficiency bonus to ability checks you make to play a game with that set. Each type of gaming set requires a separate proficiency."
    ]
  },
  {
    index: 180,
    name: "Bagpipes",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.MusicalInstrument,
    cost: {
      quantity: 30,
      unit: Unit.Gp
    },
    weight: 6,
    desc: [
      "Several of the most common types of musical instruments are shown on the table as examples. If you have proficiency with a given musical instrument, you can add your proficiency bonus to any ability checks you make to play music with the instrument. A bard can use a musical instrument as a spellcasting focus. Each type of musical instrument requires a separate proficiency."
    ]
  },
  {
    index: 181,
    name: "Drum",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.MusicalInstrument,
    cost: {
      quantity: 6,
      unit: Unit.Gp
    },
    weight: 3,
    desc: [
      "Several of the most common types of musical instruments are shown on the table as examples. If you have proficiency with a given musical instrument, you can add your proficiency bonus to any ability checks you make to play music with the instrument. A bard can use a musical instrument as a spellcasting focus. Each type of musical instrument requires a separate proficiency."
    ]
  },
  {
    index: 182,
    name: "Dulcimer",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.MusicalInstrument,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    weight: 10,
    desc: [
      "Several of the most common types of musical instruments are shown on the table as examples. If you have proficiency with a given musical instrument, you can add your proficiency bonus to any ability checks you make to play music with the instrument. A bard can use a musical instrument as a spellcasting focus. Each type of musical instrument requires a separate proficiency."
    ]
  },
  {
    index: 183,
    name: "Flute",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.MusicalInstrument,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 1,
    desc: [
      "Several of the most common types of musical instruments are shown on the table as examples. If you have proficiency with a given musical instrument, you can add your proficiency bonus to any ability checks you make to play music with the instrument. A bard can use a musical instrument as a spellcasting focus. Each type of musical instrument requires a separate proficiency."
    ]
  },
  {
    index: 184,
    name: "Lute",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.MusicalInstrument,
    cost: {
      quantity: 35,
      unit: Unit.Gp
    },
    weight: 2,
    desc: [
      "Several of the most common types of musical instruments are shown on the table as examples. If you have proficiency with a given musical instrument, you can add your proficiency bonus to any ability checks you make to play music with the instrument. A bard can use a musical instrument as a spellcasting focus. Each type of musical instrument requires a separate proficiency."
    ]
  },
  {
    index: 185,
    name: "Lyre",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.MusicalInstrument,
    cost: {
      quantity: 30,
      unit: Unit.Gp
    },
    weight: 2,
    desc: [
      "Several of the most common types of musical instruments are shown on the table as examples. If you have proficiency with a given musical instrument, you can add your proficiency bonus to any ability checks you make to play music with the instrument. A bard can use a musical instrument as a spellcasting focus. Each type of musical instrument requires a separate proficiency."
    ]
  },
  {
    index: 186,
    name: "Horn",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.MusicalInstrument,
    cost: {
      quantity: 3,
      unit: Unit.Gp
    },
    weight: 2,
    desc: [
      "Several of the most common types of musical instruments are shown on the table as examples. If you have proficiency with a given musical instrument, you can add your proficiency bonus to any ability checks you make to play music with the instrument. A bard can use a musical instrument as a spellcasting focus. Each type of musical instrument requires a separate proficiency."
    ]
  },
  {
    index: 187,
    name: "Pan flute",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.MusicalInstrument,
    cost: {
      quantity: 12,
      unit: Unit.Gp
    },
    weight: 2,
    desc: [
      "Several of the most common types of musical instruments are shown on the table as examples. If you have proficiency with a given musical instrument, you can add your proficiency bonus to any ability checks you make to play music with the instrument. A bard can use a musical instrument as a spellcasting focus. Each type of musical instrument requires a separate proficiency."
    ]
  },
  {
    index: 188,
    name: "Shawm",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.MusicalInstrument,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 1,
    desc: [
      "Several of the most common types of musical instruments are shown on the table as examples. If you have proficiency with a given musical instrument, you can add your proficiency bonus to any ability checks you make to play music with the instrument. A bard can use a musical instrument as a spellcasting focus. Each type of musical instrument requires a separate proficiency."
    ]
  },
  {
    index: 189,
    name: "Viol",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.MusicalInstrument,
    cost: {
      quantity: 30,
      unit: Unit.Gp
    },
    weight: 1,
    desc: [
      "Several of the most common types of musical instruments are shown on the table as examples. If you have proficiency with a given musical instrument, you can add your proficiency bonus to any ability checks you make to play music with the instrument. A bard can use a musical instrument as a spellcasting focus. Each type of musical instrument requires a separate proficiency."
    ]
  },
  {
    index: 190,
    name: "Navigator’s tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.OtherTools,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    weight: 2,
    desc: [
      "This set of instruments is used for navigation at sea. Proficiency with navigator’s tools lets you chart a ship’s course and follow navigation charts. In addition, these tools allow you to add your proficiency bonus to any ability check you make to avoid getting lost at sea."
    ]
  },
  {
    index: 191,
    name: "Thieves’ tools",
    equipment_category: EquipmentCategory.Tools,
    tool_category: ToolCategory.OtherTools,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    weight: 1,
    desc: [
      "This set of tools includes a small file, a set of lock picks, a small mirror mounted on a metal handle, a set of narrow-bladed scissors, and a pair of pliers. Proficiency with these tools lets you add your proficiency bonus to any ability checks you make to disarm traps or open locks."
    ]
  },
  {
    index: 192,
    name: "Camel",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.MountsAndOtherAnimals,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    },
    speed: {
      quantity: 50,
      unit: Unit.FtRound
    },
    capacity: "480 lb."
  },
  {
    index: 193,
    name: "Donkey",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.MountsAndOtherAnimals,
    cost: {
      quantity: 8,
      unit: Unit.Gp
    },
    speed: {
      quantity: 40,
      unit: Unit.FtRound
    },
    capacity: "420 lb."
  },
  {
    index: 194,
    name: "Mule",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.MountsAndOtherAnimals,
    cost: {
      quantity: 8,
      unit: Unit.Gp
    },
    speed: {
      quantity: 40,
      unit: Unit.FtRound
    },
    capacity: "420 lb."
  },
  {
    index: 195,
    name: "Elephant",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.MountsAndOtherAnimals,
    cost: {
      quantity: 200,
      unit: Unit.Gp
    },
    speed: {
      quantity: 40,
      unit: Unit.FtRound
    },
    capacity: "1,320 lb."
  },
  {
    index: 196,
    name: "Horse, draft",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.MountsAndOtherAnimals,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    },
    speed: {
      quantity: 40,
      unit: Unit.FtRound
    },
    capacity: "540 lb."
  },
  {
    index: 197,
    name: "Horse, riding",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.MountsAndOtherAnimals,
    cost: {
      quantity: 75,
      unit: Unit.Gp
    },
    speed: {
      quantity: 60,
      unit: Unit.FtRound
    },
    capacity: "480 lb."
  },
  {
    index: 198,
    name: "Mastiff",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.MountsAndOtherAnimals,
    cost: {
      quantity: 25,
      unit: Unit.Gp
    },
    speed: {
      quantity: 40,
      unit: Unit.FtRound
    },
    capacity: "195 lb."
  },
  {
    index: 199,
    name: "Pony",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.MountsAndOtherAnimals,
    cost: {
      quantity: 30,
      unit: Unit.Gp
    },
    speed: {
      quantity: 40,
      unit: Unit.FtRound
    },
    capacity: "225 lb."
  },
  {
    index: 200,
    name: "Warhorse",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.MountsAndOtherAnimals,
    cost: {
      quantity: 400,
      unit: Unit.Gp
    },
    speed: {
      quantity: 60,
      unit: Unit.FtRound
    },
    capacity: "540 lb."
  },
  {
    index: 201,
    name: "Barding: Padded",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 20,
      unit: Unit.Gp
    },
    weight: 16,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 202,
    name: "Barding: Leather",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 40,
      unit: Unit.Gp
    },
    weight: 20,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 203,
    name: "Barding: Studded Leather",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 180,
      unit: Unit.Gp
    },
    weight: 26,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 204,
    name: "Barding: Hide",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 40,
      unit: Unit.Gp
    },
    weight: 24,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 205,
    name: "Barding: Chain shirt",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 200,
      unit: Unit.Gp
    },
    weight: 40,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 206,
    name: "Barding: Scale mail",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 200,
      unit: Unit.Gp
    },
    weight: 90,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 207,
    name: "Barding: Breastplate",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 1600,
      unit: Unit.Gp
    },
    weight: 40,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 208,
    name: "Barding: Half plate",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 3000,
      unit: Unit.Gp
    },
    weight: 80,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 209,
    name: "Barding: Ring mail",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 12,
      unit: Unit.Gp
    },
    weight: 80,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 210,
    name: "Barding: Chain mail",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 300,
      unit: Unit.Gp
    },
    weight: 110,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 211,
    name: "Barding: Splint",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 800,
      unit: Unit.Gp
    },
    weight: 120,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 212,
    name: "Barding: Plate",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 6000,
      unit: Unit.Gp
    },
    weight: 130,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 213,
    name: "Bit and bridle",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 1
  },
  {
    index: 214,
    name: "Carriage",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 100,
      unit: Unit.Gp
    },
    weight: 600
  },
  {
    index: 215,
    name: "Cart",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 15,
      unit: Unit.Gp
    },
    weight: 200
  },
  {
    index: 216,
    name: "Chariot",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 250,
      unit: Unit.Gp
    },
    weight: 100
  },
  {
    index: 217,
    name: "Animal Feed (1 day)",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 5,
      unit: Unit.Cp
    },
    weight: 10
  },
  {
    index: 218,
    name: "Saddle, Exotic",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 60,
      unit: Unit.Gp
    },
    weight: 50,
    desc: [
      "A military saddle braces the rider, helping you keep your seat on an active mount in battle. It gives you advantage on any check you make to remain mounted. An exotic saddle is required for riding any aquatic or flying mount."
    ]
  },
  {
    index: 219,
    name: "Saddle, Military",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 20,
      unit: Unit.Gp
    },
    weight: 30,
    desc: [
      "A military saddle braces the rider, helping you keep your seat on an active mount in battle. It gives you advantage on any check you make to remain mounted. An exotic saddle is required for riding any aquatic or flying mount."
    ]
  },
  {
    index: 220,
    name: "Saddle, Pack",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 15,
    desc: [
      "A military saddle braces the rider, helping you keep your seat on an active mount in battle. It gives you advantage on any check you make to remain mounted. An exotic saddle is required for riding any aquatic or flying mount."
    ]
  },
  {
    index: 221,
    name: "Riding",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 25
  },
  {
    index: 222,
    name: "Saddlebags",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 4,
      unit: Unit.Gp
    },
    weight: 8
  },
  {
    index: 223,
    name: "Sled",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 20,
      unit: Unit.Gp
    },
    weight: 300
  },
  {
    index: 224,
    name: "Stabling (1 day)",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 5,
      unit: Unit.SP
    },
    weight: 0
  },
  {
    index: 225,
    name: "Wagon",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 35,
      unit: Unit.Gp
    },
    weight: 400
  },
  {
    index: 226,
    name: "Barding: Padded",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 20,
      unit: Unit.Gp
    },
    weight: 16,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 227,
    name: "Barding: Leather",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 40,
      unit: Unit.Gp
    },
    weight: 20,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 228,
    name: "Barding: Studded Leather",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 180,
      unit: Unit.Gp
    },
    weight: 26,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 229,
    name: "Barding: Hide",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 40,
      unit: Unit.Gp
    },
    weight: 24,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 230,
    name: "Barding: Chain shirt",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 200,
      unit: Unit.Gp
    },
    weight: 40,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 231,
    name: "Barding: Scale mail",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 200,
      unit: Unit.Gp
    },
    weight: 90,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 232,
    name: "Barding: Breastplate",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 1600,
      unit: Unit.Gp
    },
    weight: 40,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 233,
    name: "Barding: Half plate",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 3000,
      unit: Unit.Gp
    },
    weight: 80,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 234,
    name: "Barding: Ring mail",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 12,
      unit: Unit.Gp
    },
    weight: 80,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 235,
    name: "Barding: Chain mail",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 300,
      unit: Unit.Gp
    },
    weight: 110,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 236,
    name: "Barding: Splint",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 800,
      unit: Unit.Gp
    },
    weight: 120,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 237,
    name: "Barding: Plate",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 6000,
      unit: Unit.Gp
    },
    weight: 130,
    desc: [
      "Barding is armor designed to protect an animal’s head, neck, chest, and body. Any type of armor shown on the Armor table can be purchased as barding. The cost is four times the equivalent armor made for humanoids, and it weighs twice as much."
    ]
  },
  {
    index: 238,
    name: "Bit and bridle",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 2,
      unit: Unit.Gp
    },
    weight: 0
  },
  {
    index: 239,
    name: "Carriage",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 100,
      unit: Unit.Gp
    },
    weight: 60
  },
  {
    index: 240,
    name: "Cart",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 15,
      unit: Unit.Gp
    },
    weight: 20
  },
  {
    index: 241,
    name: "Chariot",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 250,
      unit: Unit.Gp
    },
    weight: 10
  },
  {
    index: 242,
    name: "Animal Feed (1 day)",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 5,
      unit: Unit.Cp
    },
    weight: 1
  },
  {
    index: 243,
    name: "Saddle, Exotic",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 60,
      unit: Unit.Gp
    },
    weight: 50,
    desc: [
      "A military saddle braces the rider, helping you keep your seat on an active mount in battle. It gives you advantage on any check you make to remain mounted. An exotic saddle is required for riding any aquatic or flying mount."
    ]
  },
  {
    index: 244,
    name: "Saddle, Military",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 20,
      unit: Unit.Gp
    },
    weight: 30,
    desc: [
      "A military saddle braces the rider, helping you keep your seat on an active mount in battle. It gives you advantage on any check you make to remain mounted. An exotic saddle is required for riding any aquatic or flying mount."
    ]
  },
  {
    index: 245,
    name: "Saddle, Pack",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 5,
      unit: Unit.Gp
    },
    weight: 15,
    desc: [
      "A military saddle braces the rider, helping you keep your seat on an active mount in battle. It gives you advantage on any check you make to remain mounted. An exotic saddle is required for riding any aquatic or flying mount."
    ]
  },
  {
    index: 246,
    name: "Riding",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 10,
      unit: Unit.Gp
    },
    weight: 2
  },
  {
    index: 247,
    name: "Saddlebags",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 4,
      unit: Unit.Gp
    },
    weight: 0
  },
  {
    index: 248,
    name: "Sled",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 20,
      unit: Unit.Gp
    },
    weight: 30
  },
  {
    index: 249,
    name: "Stabling (1 day)",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 5,
      unit: Unit.SP
    },
    weight: 0
  },
  {
    index: 250,
    name: "Wagon",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.TackHarnessAndDrawnVehicles,
    cost: {
      quantity: 35,
      unit: Unit.Gp
    },
    weight: 40
  },
  {
    index: 251,
    name: "Galley",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.WaterborneVehicles,
    cost: {
      quantity: 30000,
      unit: Unit.Gp
    },
    speed: {
      quantity: 4,
      unit: Unit.Mph
    }
  },
  {
    index: 252,
    name: "Keelboat",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.WaterborneVehicles,
    cost: {
      quantity: 3000,
      unit: Unit.Gp
    },
    speed: {
      quantity: 1,
      unit: Unit.Mph
    },
    desc: [
      "Keelboats and rowboats are used on lakes and rivers. If going downstream, add the speed of the current (typically 3 miles per hour) to the speed of the vehicle. These vehicles can’t be rowed against any significant current, but they can be pulled upstream by draft animals on the shores. A rowboat weighs 100 pounds, in case adventurers carry it over land."
    ]
  },
  {
    index: 253,
    name: "Longship",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.WaterborneVehicles,
    cost: {
      quantity: 10000,
      unit: Unit.Gp
    },
    speed: {
      quantity: 3,
      unit: Unit.Mph
    }
  },
  {
    index: 254,
    name: "Rowboat",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.WaterborneVehicles,
    cost: {
      quantity: 50,
      unit: Unit.Gp
    },
    speed: {
      quantity: 1.5,
      unit: Unit.Mph
    },
    desc: [
      "Keelboats and rowboats are used on lakes and rivers. If going downstream, add the speed of the current (typically 3 miles per hour) to the speed of the vehicle. These vehicles can’t be rowed against any significant current, but they can be pulled upstream by draft animals on the shores. A rowboat weighs 100 pounds, in case adventurers carry it over land."
    ]
  },
  {
    index: 255,
    name: "Sailing ship",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.WaterborneVehicles,
    cost: {
      quantity: 10000,
      unit: Unit.Gp
    },
    speed: {
      quantity: 2,
      unit: Unit.Mph
    }
  },
  {
    index: 256,
    name: "Warship",
    equipment_category: EquipmentCategory.MountsAndVehicles,
    vehicle_category: VehicleCategory.WaterborneVehicles,
    cost: {
      quantity: 25000,
      unit: Unit.Gp
    },
    speed: {
      quantity: 2.5,
      unit: Unit.Mph
    }
  }
];
