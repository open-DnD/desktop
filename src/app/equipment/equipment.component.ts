import { Component, OnInit } from '@angular/core';
import { Equipment } from "./equipment";
import { EquipmentService } from "./equipment.service";

@Component({
  selector: 'app-equipment',
  templateUrl: './equipment.component.html',
  styleUrls: ['./equipment.component.css']
})
export class EquipmentComponent implements OnInit {
  items: Equipment[];
  selecteditem: Equipment;

  constructor(private itemService: EquipmentService) {}

  ngOnInit() {
    this.getItems();
  }

  onSelect(item: Equipment): void {
    this.selecteditem = item;
  }

  getItems(): void {
    this.itemService.getItems().subscribe(items => (this.items = items));
  }

}
