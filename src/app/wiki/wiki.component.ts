import { Component, OnInit, Input } from "@angular/core";
import { Wiki } from "./wiki";
import { WikiService } from "./wiki.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-wiki",
  templateUrl: "./wiki.component.html",
  styleUrls: ["./wiki.component.css"]
})
export class WikiComponent implements OnInit {
  wikis: Wiki;
  id: number;

  constructor(private wikiService: WikiService, private route: ActivatedRoute) {}

  getWikis(): void {
    this.wikiService.getWikiID(this.id).subscribe(wikis => (this.wikis = wikis));
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get("id");
    this.getWikis();
  }
}
