import { Component, OnInit } from "@angular/core";
import { Wiki } from "../wiki";
import { WikiService } from "../wiki.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-category",
  templateUrl: "./category.component.html",
  styleUrls: ["./category.component.css"]
})
export class CategoryComponent implements OnInit {
  wikis: Wiki;
  constructor(private wikiService: WikiService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.getWikis();
  }

  getWikis(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    const category = this.route.snapshot.paramMap.get("category");
    this.wikiService.getWikiCategory(id, category).subscribe(wikis => (this.wikis = wikis));
  }
}
