import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-timeline-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.css"]
})
export class TimelineNavComponent implements OnInit {
  campaignid: number;
  constructor(private location: Location, private route: ActivatedRoute) {}

  ngOnInit() {
    this.campaignid = +this.route.snapshot.paramMap.get("id");
  }

  back() {
    this.location.back();
  }
}
