import { Component, OnInit } from "@angular/core";
import { WikiService } from "../../wiki.service";
import { ActivatedRoute } from "@angular/router";
import { Wiki } from "../../wiki";

@Component({
  selector: "app-timeline-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["../timeline.component.css"]
})
export class TimelineEditComponent implements OnInit {
  campaignid: number;
  wikis: Wiki;

  constructor(private wikiService: WikiService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.campaignid = +this.route.snapshot.paramMap.get("id");
    this.getWikis();
  }

  getWikis(): void {
    this.wikiService.getWikiID(this.campaignid).subscribe(wikis => (this.wikis = wikis));
  }
}
