import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelineNewComponent } from './new.component';

describe('NewComponent', () => {
  let component: TimelineNewComponent;
  let fixture: ComponentFixture<TimelineNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
