import { Component, OnInit } from "@angular/core";
import { Timeline } from "../timeline";
import { Location } from "@angular/common";
import { WikiService } from "../wiki.service";
import { ActivatedRoute } from "@angular/router";
import { Wiki } from "../wiki";

@Component({
  selector: "app-timeline",
  templateUrl: "./timeline.component.html",
  styleUrls: ["./timeline.component.css"]
})
export class TimelineComponent implements OnInit {
  campaignid: number;
  wikis: Wiki;
  constructor(
    private location: Location,
    private wikiService: WikiService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.campaignid = +this.route.snapshot.paramMap.get("id");
    this.getWikis();
  }

  getWikis(): void {
    this.wikiService.getWikiID(this.campaignid).subscribe(wikis => (this.wikis = wikis));
  }

  back() {
    this.location.back();
  }
}
