import { Component, OnInit, Input } from "@angular/core";
import { Wiki } from "../wiki";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-wiki-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.css"]
})
export class NavComponent implements OnInit {
  @Input() wikis: Wiki;
  campaignid: number;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.campaignid = +this.route.snapshot.paramMap.get("id");
  }
}
