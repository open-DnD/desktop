// Title is the Pages title
// Description gives the user the abiltiy to define what this wiki is about
// Sidemenu gives the user the ability to add custom sidemenu items
// Sections gives the user the abiltiy to add custom sections about the page
export class Wiki {
  campaign_id: Number;
  pages: Page[];
  title: string;
  description: string;
  sidemenu?: string;
  timeline?: Timeline;
  map?: Map;
  sections: Section[];
}
// Title is the Pages title
// Description is the Pages Description
// Map_id, Item_id etc.) Link page with map or item
// Category is what type of page is this for
// visability is a varible deciding if the Page will be visible
// Sections gives the user the abiltiy to add custom sections about the page
export class Page {
  title?: string;
  description?: string;
  map_id?: number;
  item_id?: number;
  campaign_id?: number;
  spell_id?: number;
  monster_id?: number;
  character_id?: number;
  category: "character" | "spells" | "items" | "locations" | "monsters";
  visibility: boolean;
  sections: Section[];
}

// Title is the sections title
// Body is the sections Body
// visability is a varible deciding if the section will be visible
// Position is where the section will be in relation is to other custom sections
export class Section {
  title: string;
  body: string;
  visibility: boolean;
  position: Number;
}

export class TSection {
  title: string;
  body: string;
  visibility: boolean;
  position: "left" | "right" | "center";
}

// Title is the Timeline's title
// Body is the Timeline's Body
// events is a collection of class sections as they share the same structure
export class Timeline {
  title: string;
  body: string;
  events: TSection[];
}

// Title is the Map's title
// Body is the Map's Body
// Locations is a collection of class Location
export class Map {
  title: string;
  body: string;
  locations: Location[];
  picture: string;
}

// Title is the Location's title
// Body is the Location's Body
// visability is a varible deciding if the Location will be visible
// X is locations x postion
// Y is locations y postion
export class Location {
  title: string;
  body: string;
  visibility: boolean;
  X: Number;
  Y: Number;
  enemies?: number[];
  characters?: number[];
  notes?: string;
}
