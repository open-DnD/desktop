import { Injectable } from "@angular/core";

import { Wiki, Page } from "./wiki";
import { Wikis } from "./mock-wiki";

import { Observable, of } from "rxjs";
import { MessageService } from "../message.service";

@Injectable({
  providedIn: "root"
})
export class WikiService {
  constructor(private messageService: MessageService) {}

  // Get Wiki by ID
  getWikiID(id: number): Observable<Wiki> {
    this.messageService.add(`WikiService: fetched campaign id=${id}`);
    return of(Wikis.find(wiki => wiki.campaign_id === id));
  }

  // Get Wiki by Category
  getWikiCategory(id: number, category: string): Observable<Wiki> {
    this.messageService.add(`WikiService: fetched campaign by category=${category}`);
    let PageCopy: Page[];
    let WikiCopy: Wiki;
    WikiCopy = Wikis.find(wiki => wiki.campaign_id === id);
    WikiCopy.pages.forEach(page => {
      if (page.category === category) {
        PageCopy.push(page);
      }
    });
    WikiCopy.pages = PageCopy;
    return of(WikiCopy);
  }

  // Get All Wikis
  getWikis(): Observable<Wiki[]> {
    this.messageService.add("WikiService: fetched campaigns");
    return of(Wikis);
  }
}
