export class Pointer {
  id: number;
  x: number;
  y: string;
  icon_type?: string;
  subtitle: string;
}
