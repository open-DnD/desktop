import { Wiki } from "./wiki";

export const Wikis: Wiki[] = [
  {
    campaign_id: 1,
    title: "Wiki For Campaign",
    pages: [
      {
        title: "$Character Name",
        description: "$Character Discription",
        character_id: 1,
        visibility: true,
        category: "character",
        sections: [{ title: "Test", body: "", visibility: true, position: 1 }]
      },
      {
        title: "$Character Name",
        description: "$Character Discription",
        character_id: 2,
        visibility: true,
        category: "character",
        sections: [{ title: "Test", body: "", visibility: true, position: 1 }]
      },
      {
        title: "$Spell Name",
        description: "$Spell Discription",
        spell_id: 1,
        visibility: true,
        category: "spells",
        sections: [{ title: "Test", body: "", visibility: true, position: 1 }]
      }
    ],
    sections: [{ title: "Introduction", body: "Cause who knows", visibility: true, position: 1 }],
    description: "cause you know people may want custom wiki for the campaign",
    timeline: {
      title: "Campaign Timeline",
      body: "Incase there is a body people want about the timeline",
      events: [
        { title: "2017", body: "Lorem ipsum..One", visibility: true,  position: "left" },
        { title: "2016", body: "Lorem ipsum..Two", visibility: true, position: "right" }
      ]
    },
    map: {
      title: "",
      body: "",
      picture: "assests/images/map.jpg",
      locations: [
        {
          title: "Tavern",
          body: "",
          visibility: true,
          X: 12,
          Y: 12,
          characters: [1, 2],
          enemies: [],
          notes: ""
        },
        {
          title: "Shops",
          body: "",
          visibility: true,
          X: 5,
          Y: 7,
          characters: [1],
          enemies: [],
          notes: ""
        }
      ]
    }
  }
];
