export class Timeline {
  campaign_id: number;
  date?: string;
  title: string;
  description: string;
  colour: string;
  tags?: string[];
  postition?: string;
}
