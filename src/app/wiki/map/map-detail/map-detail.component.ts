import { Component, OnInit, Input } from "@angular/core";
import { Map } from "../../wiki";

@Component({
  selector: "app-map-detail",
  templateUrl: "./map-detail.component.html",
  styleUrls: ["./map-detail.component.css"]
})
export class MapDetailComponent implements OnInit {
  constructor() {}

  @Input()
  map: Map;

  ngOnInit() {}
}
