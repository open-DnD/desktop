import { Component, OnInit, Input } from "@angular/core";
import { WikiService } from "../wiki.service";
import { Wiki, Map } from "../wiki";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.css"]
})
export class MapComponent implements OnInit {
  campaignid: number;
  wikis: Wiki;
  selectedMap: Map;

  constructor(private wikiService: WikiService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.campaignid = +this.route.snapshot.paramMap.get("id");
    this.getWikis();
  }

  getWikis(): void {
    this.wikiService.getWikiID(this.campaignid).subscribe(wikis => (this.wikis = wikis));
  }

  onSelect(map: Map): void {
    this.selectedMap = map;
  }
}
