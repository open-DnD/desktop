import { Injectable } from "@angular/core";

import { Observable, of } from "rxjs";

import { Monster } from "./monster";
import { MONSTERS } from "./mock-monsters";

import { MessageService } from "../message.service";

@Injectable({
  providedIn: "root"
})
export class MonsterService {
  constructor(private messageService: MessageService) {}

  getMonsters(): Observable<Monster[]> {
    // TODO: send the message _after_ fetching the heroes
    this.messageService.add("MonsterService: fetched monsters");
    return of(MONSTERS);
  }

  getMonster(id: number): Observable<Monster> {
    this.messageService.add(`CampaignService: fetched monster id=${id}`);
    return of(MONSTERS.find(monster => monster.index === id));
  }

  addMonster() {}

  removeMonster(id: Number) {}
}
