import { Component, OnInit } from "@angular/core";
import { Monster } from "./monster";
import { MonsterService } from "./monster.service";

@Component({
  selector: "app-monsters",
  templateUrl: "./monsters.component.html",
  styleUrls: ["./monsters.component.css"]
})
export class MonstersComponent implements OnInit {
  monsters: Monster[];
  selectedMonster: Monster;

  constructor(private monsterService: MonsterService) {}

  getMonsters(): void {
    this.monsterService.getMonsters().subscribe(monsters => (this.monsters = monsters));
  }

  ngOnInit() {
    this.getMonsters();
  }
}
