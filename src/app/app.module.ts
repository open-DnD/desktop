import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FlexLayoutModule } from "@angular/flex-layout";
import { MaterialModule } from "./material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CharactersComponent } from "./characters/characters.component";
import { CharacterDetailComponent } from "./characters/character-detail/character-detail.component";
import { MonstersComponent } from "./monsters/monsters.component";
import { MonsterDetailComponent } from "./monsters/monster-detail/monster-detail.component";
import { CampaignsComponent } from "./campaigns/campaigns.component";
import { CampaignDetailComponent } from "./campaigns/campaign-detail/campaign-detail.component";
import { MessagesComponent } from "./messages/messages.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { HelpComponent } from "./help/help.component";
import { WikiComponent } from "./wiki/wiki.component";
import { WikiDetailComponent } from "./wiki/wiki-detail/wiki-detail.component";
import { TimelineComponent } from "./wiki/timeline/timeline.component";
import { CategoryComponent } from "./wiki/category/category.component";
import { NavComponent } from "./wiki/nav/nav.component";
import { MapComponent } from "./wiki/map/map.component";
import { MapDetailComponent } from "./wiki/map/map-detail/map-detail.component";
import { MapViewComponent } from "./wiki/map/map-view/map-view.component";
import { TimelineEditComponent } from "./wiki/timeline/edit/edit.component";
import { TimelineNewComponent } from "./wiki/timeline/new/new.component";
import { TimelineNavComponent } from "./wiki/timeline/nav/nav.component";
import { EquipmentComponent } from "./equipment/equipment.component";
import { SpellsComponent } from "./spells/spells.component";
import { EquipmentDetailComponent } from './equipment/equipment-detail/equipment-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    CharactersComponent,
    MonstersComponent,
    CampaignsComponent,
    CampaignDetailComponent,
    MessagesComponent,
    DashboardComponent,
    CharacterDetailComponent,
    MonsterDetailComponent,
    HelpComponent,
    WikiComponent,
    WikiDetailComponent,
    TimelineComponent,
    CategoryComponent,
    TimelineNavComponent,
    NavComponent,
    MapComponent,
    MapDetailComponent,
    MapViewComponent,
    TimelineEditComponent,
    TimelineNewComponent,
    EquipmentComponent,
    SpellsComponent,
    EquipmentDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MaterialModule,
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
