import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { EquipmentComponent } from "./equipment/equipment.component";
import { EquipmentDetailComponent } from "./equipment/equipment-detail/equipment-detail.component";
import { CampaignsComponent } from "./campaigns/campaigns.component";
import { CampaignDetailComponent } from "./campaigns/campaign-detail/campaign-detail.component";
import { MonstersComponent } from "./monsters/monsters.component";
import { MonsterDetailComponent } from "./monsters/monster-detail/monster-detail.component";
import { CharactersComponent } from "./characters/characters.component";
import { CharacterDetailComponent } from "./characters/character-detail/character-detail.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { WikiComponent } from "./wiki/wiki.component";
import { CategoryComponent } from "./wiki/category/category.component";
import { MapComponent } from "./wiki/map/map.component";
import { TimelineComponent } from "./wiki/timeline/timeline.component";
import { TimelineEditComponent } from "./wiki/timeline/edit/edit.component";
import { TimelineNewComponent } from "./wiki/timeline/new/new.component";

const routes: Routes = [
  { path: "", component: DashboardComponent },
  { path: "campaign/:id", component: CampaignDetailComponent },
  { path: "campaign/wiki/:id", component: WikiComponent },
  { path: "campaign/wiki/:id/map", component: MapComponent },
  { path: "campaign/wiki/:id/timeline", component: TimelineComponent },
  { path: "campaign/wiki/:id/timeline/edit", component: TimelineEditComponent },
  { path: "campaign/wiki/:id/timeline/new", component: TimelineNewComponent },
  { path: "campaign/wiki/:id/:category", component: CategoryComponent },

  { path: "campaigns", component: CampaignsComponent },
  { path: "items", component: EquipmentComponent },
  { path: "item/:id", component: EquipmentDetailComponent },
  { path: "monsters", component: MonstersComponent },
  { path: "monster/:id", component: MonsterDetailComponent },
  { path: "characters", component: CharactersComponent },
  { path: "character/:id", component: CharacterDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
